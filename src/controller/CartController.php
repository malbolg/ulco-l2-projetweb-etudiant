<?php namespace controller;


use model\StoreModel;
class CartController {

    public static function get(): void {
        $params= [
            "title" => "Votre panier",
            "module" => "cart.php"
        ];

        \view\Template::render($params);

    }


    public static function push(): void {
        if( isset( $_POST[ "quantity" ] ) && isset( $_POST[ "PID" ] ) ) {

            $_POST[ "quantity" ]= intval( $_POST[ "quantity" ] );
            $_POST[ "PID" ]= htmlspecialchars( $_POST[ "PID" ] );

            $data= StoreModel::mininfoProduct( $_POST[ "PID" ] );
            if( empty( $data ) ) { return; }
            session_start();
            $_SESSION[ "cart_content" ][ $_POST[ "PID" ] ]= array(
                "quantity" => $_POST[ "quantity" ],
                "productinf" => $data
            );

            self::get();
        }
    }


}
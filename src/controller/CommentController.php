<?php namespace controller;
use Model\CommentModel;

class CommentController {

    public function postComment(): void {
        session_start();
        $_POST[ "PID" ]= intval( htmlspecialchars( $_POST[ "PID" ] ));

        CommentModel::postComment(
            $_POST[ "PID" ],
            $_SESSION[ "UID" ],
            htmlspecialchars( $_POST[ "comment_data" ] )
        );

        header( "Location: /store/".$_POST[ "PID" ], true );
        exit();
    }

}
<?php namespace model;

class AccountModel {

    public static function check( ?String $firstname, ?String $lastname, ?String $mail, ?String $password ): bool {

        $db= Model::connect();

        $qr= "SELECT mail FROM account
                   WHERE mail = '" . $mail. "'";

        $res= $db->prepare( $qr );
        $res->execute();

        if( $res->fetch() != false ) return false;

        return strlen( $firstname ) > 1
            && strlen( $lastname ) > 1
            && filter_var( $mail, FILTER_VALIDATE_EMAIL )
            && preg_match( "/(?=^(.{5,}[a-z]))(?=^(.*[0-9]))/", $password);
    }

    public static function canModify( ?String $firstname, ?String $lastname, ?String $mail ) {
        $db= Model::connect();

        $verify= "SELECT mail, id FROM account
                   WHERE mail = '".$mail."' AND id != ".($_SESSION[ "UID" ] or -1);

        $res= $db->prepare( $verify );
        $res->execute();

        if( $res->fetch() != false ) return false;

        return strlen( $firstname ) > 1
            && strlen( $lastname ) > 1
            && filter_var( $mail, FILTER_VALIDATE_EMAIL );
    }


    public static function signin( ?String $firstname, ?String $lastname, ?String $mail, ?String $password ): bool {


        if( !self::canModify( $firstname, $lastname, $mail, $password ) ) return false;


        $db= Model::connect();


        $res= $db->prepare(
            "INSERT INTO account ( firstname, lastname, mail, password ) 
                VALUES ( '" .$firstname.
            "', '" .$lastname .
            "', '" .$mail .
            "', '" . password_hash( $password, PASSWORD_DEFAULT ) .
            "' );"
        );

        $res->execute();



        return true;
    }

    public static function login( ?String $mail, ?String $password ): bool {
        $db= Model::connect();

        $res= $db->prepare( "SELECT mail, password, firstname, lastname, id FROM account
                                    WHERE mail= '" .$mail. "'" );
        $res->execute();

        if( ($ans= $res->fetch()) == false ) return false;
        if( !password_verify( $password, $ans[ "password" ] ) ) return false;

        session_start();
        $_SESSION[ "user" ]= $ans[ "firstname" ] ." ". $ans[ "lastname" ];
        $_SESSION[ "UID" ]= $ans[ "id" ];

        return true;
    }

    public static function retrieveAccountInfos( int $UID ) {
        $accFetch= "SELECT id, firstname, lastname, mail FROM account WHERE id = ".$UID;
        $DB= Model::connect();

        $get= $DB->prepare( $accFetch );
        $get->execute();
        $ans= $get->fetch();


        return $ans != false? $ans : null;
    }

    public static function update( array $updates= null ): bool {

        if( !self::canModify( $updates[ "firstname" ], $updates[ "lastname" ], $updates[ "email" ] ) ) return false;

        $req= "UPDATE account
                SET 
                    firstname= \"".$updates[ "firstname" ]."\",
                    lastname = \"".$updates[ "lastname" ]."\",
                    mail= \"".$updates[ "email" ]."\"
                WHERE id = ".$_SESSION[ "UID" ].";";

        $DB= Model::connect();
        $query= $DB->prepare( $req );
        $query->execute();

        return true;
    }

}



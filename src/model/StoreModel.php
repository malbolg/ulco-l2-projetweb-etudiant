<?php

namespace model;

class StoreModel {

    static function listCategories(): array
    {
        // Connexion à la base de données
        $db = Model::connect();

        // Requête SQL
        $sql = "SELECT id, name FROM category";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();

        // Retourner les résultats (type array)
        return $req->fetchAll();
    }

    static function listProducts( ?String $s= null): array {

        $db= Model::connect();

        $fetch= "SELECT P.id, P.name, P.price, P.image, V.name AS category FROM product P
                            INNER JOIN category V on P.category = V.id";

        $res= $db->prepare( $fetch );
        $res->execute();

        return $res->fetchAll();
    }

    static function infoProduct( int $id ) : ?array {

        $DB= Model::connect();

        $fetch= "SELECT P.id, P.name, P.price, P.spec,
                        P.image, P.image_alt1, P.image_alt2, P.image_alt3, 
                        V.name AS category 
                            FROM product P
                                INNER JOIN category V on P.category = V.id
                                HAVING P.id = " . $id;

        $res= $DB->prepare( $fetch );
        $res->execute();
        $ans= $res->fetch();


        return $ans != false? $ans : null;
    }

    static function minInfoProduct( int $PID ): ?array {    // similaire a infoProduct, mais retourne moins de donnée
        $DB= Model::connect();

        $fetch= "SELECT P.id, P.name, P.price, P.image, V.name AS category 
                            FROM product P
                                INNER JOIN category V on P.category = V.id
                                HAVING P.id = " . $PID;

        $res= $DB->prepare( $fetch );
        $res->execute();
        $ans= $res->fetch();


        return $ans != false? $ans : null;
    }

    static function searchProducts( String $search, array $parameters= null ) {
        $fetch= "SELECT P.id, P.name, P.price, P.image, V.name AS category FROM product P
                            INNER JOIN category V on P.category = V.id ";

        if( $parameters != null ) {

            if( isset( $parameters[ "category" ] ) ) {
                $fetch .= "HAVING V.name IN ( ";
                $fetch .= join( ", ", $parameters[ "category" ] );
                $fetch .= " ) AND P.name LIKE \"%".$search."%\" ";
            } else $fetch.= "HAVING P.name LIKE \"%".$search."%\" ";

            if( isset( $parameters[ "sort" ] ) && ($parameters[ "sort" ] == "ASC" || $parameters[ "sort" ] == "DESC") ) {
                $fetch .= "ORDER BY P.price " . $parameters[ "sort" ];
            }
        }

        $fetch .= ";";


        $DB= Model::connect();

        $res= $DB->prepare( $fetch );
        $res->execute();
        $ans= $res->fetchAll();

        return $ans != false? $ans : null;
    }
}
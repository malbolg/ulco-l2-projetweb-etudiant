<?php

if( isset( $_SESSION[ 'UID' ] ) ) {
    header( "Location: /", true );
    exit();
}



?>

    <?PHP
    if( isset( $_GET[ "status" ] ) ) {

    ?> <div class="box info box-login <?PHP

    switch( $_GET[ "status" ] ) {
        case "login_failed":
            ?>error"><pre>La connexion a échouée. veullez ré-éssayer..</pre><?php
            break;

        case "signin_success":
            ?>"><pre>Vous avez été inscript, vous pouvez désormais vous connecter.</pre><?PHP
            break;

        case "signin_failed":
            ?>error"><pre>Erreur lors de l'inscription, ré-éssayer..</pre><?PHP
            break;

        case "logout":
            ?>"><pre>Vous avez été déconnecté!</pre><?PHP
        break;
        default:
            header( "Location: /account", true );
            exit();
            //? erreur, status non reconnue par le serveur
    }

    ?></div><?PHP
}?>

<div id="account">




    <form class="account-login" method="post" action="/account/login">

      <h2>Connexion</h2>
      <h3>Tu as déjà un compte ?</h3>

      <p>Adresse mail</p>
      <input type="text" name="usermail" placeholder="Adresse mail" />

      <p>Mot de passe</p>
      <input type="password" name="userpass" placeholder="Mot de passe" />

      <input type="submit" value="Connexion" />

    </form>

    <form class="account-signin" method="post" action="/account/signin">

      <h2>Inscription</h2>
      <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

      <p>Nom</p>
      <input type="text" name="userlastname" id="lname" placeholder="Nom" />

      <p>Prénom</p>
      <input type="text" name="userfirstname" id="fname" placeholder="Prénom" />

      <p>Adresse mail</p>
      <input type="text" name="usermail" id="mail" placeholder="Adresse mail" />

      <p>Mot de passe</p>
      <input type="password" name="userpass" id="pass" placeholder="Mot de passe" />

      <p>Répéter le mot de passe</p>
      <input type="password" name="uservpass" id="passv" placeholder="Mot de passe" />

      <input type="submit" value="Inscription" />

    </form>

</div>


<script type="text/javascript" src="/public/scripts/validation.js"></script>

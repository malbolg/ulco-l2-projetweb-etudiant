<?php

    if( isset( $_GET[ "status" ] ) ) {?>
        <div class="box info box-login

        <?PHP
        switch( $_GET[ "status" ] ) {
            case "modify_success":?>
                ">Information mise à jour!</div>

            <?php
            break;

            case "modify_fail":?>
                error">Il y a eu un problème lors de la mise a jour de vos informations..</div>
            <?php
            break;

            default:?>
                error">Erreur: status serveur inconnue</div>
            <?php
        }

    }



?>




<div id="account">
    <form class="account-modify" method="post" action="/account/update">

        <h1>Informations du compte</h1>

        <p class="field">Nom</p>
        <input type="text" id="lname" name="lname" placeholder="Bush" value="<?=$params["lastname"]?>"/>


        <p class="field">Prénom</p>
        <input type="text" id="fname" name="fname" placeholder="George" value="<?=$params["firstname"]?>"/>

        <p class="field">Adresse mail</p>
        <input type="text" id="mail" name="mail" placeholder="bushgeorge@domain.something" value="<?=$params["email"]?>" />


        <input type="submit" value="Appliquer"/>
    </form>

</div>


<script type="text/javascript" src="/public/scripts/validation.js"></script>
